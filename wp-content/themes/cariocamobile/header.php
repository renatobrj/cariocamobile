<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package oquealeria
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link href="<?php bloginfo('template_url');?>/assets/css/style.css" rel="stylesheet"/>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

<header>
	<div class="header d-flex justify-content-between align-items-center">
		<h1 class="logo"><a href="<?php echo get_home_url(); ?>"><img src="<?php bloginfo('template_url');?>/assets/images/logo.png"></a></h1>
		<nav class="nav-header">
			<?php
				wp_nav_menu( array( 
				'theme_location' => 'menu-1', 
				'container' => 'false' ) ); 
			?>
		</nav>
		<div class="social -header">
			<ul>
				<li><a href=""><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
				<li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
			</ul>
		</div>
		<div class="button-nav">
			<div class="collapsed btn-burguer blue" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="icon-bar top-bar"></span>
				<span class="icon-bar middle-bar"></span>
				<span class="icon-bar bottom-bar"></span>
			</div>
		</div>
	</div>
	<div class="menu-mobile">
		<nav>
			<?php
				wp_nav_menu( array( 
				'theme_location' => 'menu-mobile', 
				'container' => 'false' ) ); 
			?>
		</nav>
	</div>
</header>
