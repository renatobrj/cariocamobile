<div class="content-banner">
	<div id="banner-destaque" class="hero section text-center parallax parallax-window" data-parallax="scroll" data-image-src="<?php bloginfo('template_url'); ?>/assets/images/banner-looks.png">
		<h2 class="title">
			<ul>
				<li data-aos="fade-right" data-aos-delay="300" data-aos-duration="400">L</li>
				<li data-aos="fade-right" data-aos-delay="500" data-aos-duration="400">O</li>
				<li data-aos="fade-right" data-aos-delay="700" data-aos-duration="400">O</li>
				<li data-aos="fade-right" data-aos-delay="900" data-aos-duration="400">K</li>
				<li data-aos="fade-right" data-aos-delay="1100" data-aos-duration="400">B</li>
				<li data-aos="fade-right" data-aos-delay="1300" data-aos-duration="400">O</li>
				<li data-aos="fade-right" data-aos-delay="1500" data-aos-duration="400">O</li>
				<li data-aos="fade-right" data-aos-delay="1700" data-aos-duration="400">K</li>
				<li data-aos="fade-right" data-aos-delay="1900" data-aos-duration="400">S</li>
			</ul>
		</h2>
		<a href="" data-aos="zoom-in-up" data-aos-delay="800" data-aos-duration="400">Ver mais</a>
	</div>
</div>