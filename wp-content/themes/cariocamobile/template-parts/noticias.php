<?php
	$args = array(
    	'post_type' => 'post',
    	'posts_per_page' => 4
	);
	
	$i = 500;
	$posthome = new WP_Query($args);
?>


<div id="noticias" class="section-conteudo">
	<div class="container">
	<h2 class="title text-center">Novidades</h2>
		<div class="content-noticias">
			<div class="row">
			<?php if ( $posthome->have_posts() ) : ?>
			<?php  while ( $posthome->have_posts() ) : $posthome->the_post(); ?>
			<div class="col-md-6 col-lg-3" data-aos="zoom-in-up" data-aos-delay="200" data-aos-duration="600">
				<?php
					$thumb_id = get_post_thumbnail_id();
					$thumb_url = wp_get_attachment_image_src($thumb_id,'full', true);
				?>
				<a href="<?php the_permalink(); ?>">
					<div class="thumb"><img src="<?php echo $thumb_url[0] ?>"></div>
				</a>
				<div class="caption">
					<p><?php $content = nl2br(get_the_content()); ?>
						<?php echo(wp_strip_all_tags(substr($content, 0, 120))); ?></p>
				</div>
				<div class="link-noticia"><a href="<?php the_permalink() ?>">Ver Mais</a></div>
			</div>
			<?php  endwhile; wp_reset_postdata();  ?>
				<?php $i++; endif; ?>
			</div>
		</div>
	</div>
</div>