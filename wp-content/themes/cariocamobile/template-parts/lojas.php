
<div id="nossas-lojas" class="hero text-center" style="background-image: url('<?php bloginfo('template_url'); ?>/assets/images/background-lojas.png');">
	
	<h2 class="title">Nossas Lojas</h2>

	<div class="slider-loja">
		<a href="<?php bloginfo('template_url'); ?>/lojas">
		<div class="item">
			<div class="title-loja">IPANEMA</div>
			<div class="end-loja"> Rua Visconde de Pirajá, 330, Lj. B</div>
			<div class="tel">Tel.: (21) 2267-5080</div>
		</div>
		</a>
		<a href="<?php bloginfo('template_url'); ?>/lojas">
		<div class="item">
			<div class="title-loja">SHOPPING LEBLON</div>
			<div class="end-loja"> Av. Afrânio de Melo Franco, 290, Lj. 303A-B</div>
			<div class="tel">Tel.: (21) 2294-0144</div>
		</div>
		</a>
		<a href="<?php bloginfo('template_url'); ?>/lojas">
		<div class="item">
			<div class="title-loja">SHOPPING VILLAGE MALL</div>
			<div class="end-loja"> Av. das Américas, 3.900, Lj. 123, piso 1</div>
			<div class="tel">Tel.: (21) 3252-2515</div>
		</div>
		</a>
		<a href="<?php bloginfo('template_url'); ?>/lojas">
		<div class="item">
			<div class="title-loja">RIO DESIGN LEBLON</div>
			<div class="end-loja"> Av. Ataulfo de Paiva, 270, Lj. 115</div>
			<div class="tel">Tel.: (21) 3204-1577</div>
		</div>
		</a>
		<a href="<?php bloginfo('template_url'); ?>/lojas">
		<div class="item">
			<div class="title-loja">RIO DESIGN BARRA</div>
			<div class="end-loja"> Av. das Américas, 7777, Lj. 122</div>
			<div class="tel">Tel.: (21) 3328-3801</div>
		</div>
		</a>
		<a href="<?php bloginfo('template_url'); ?>/lojas">
		<div class="item">
			<div class="title-loja">OUTLET PREMIUM</div>
			<div class="end-loja"> Rodovia Washington Luíz, S/N</div>
			<div class="tel">Tel.: (21) 3900-0500</div>
		</div>
		</a>
		<a href="<?php bloginfo('template_url'); ?>/lojas">
		<div class="item">
			<div class="title-loja">SHOPPING VILA OLÍMPIA</div>
			<div class="end-loja"> Rua Olimpíadas, 360, lojas 137/138</div>
			<div class="tel">Tel.: (11) 3045-0616</div>
		</div>
		</a>
		<a href="<?php bloginfo('template_url'); ?>/lojas">
		<div class="item">
			<div class="title-loja">SHOPPING ANÁLIA FRANCO</div>
			<div class="end-loja"> Av.Regente Feijó, 1739  Loja 54B piso Lírio</div>
			<div class="tel">Tel.: (11) 2268-0630</div>
		</div>
		</a>
	</div>
</div>