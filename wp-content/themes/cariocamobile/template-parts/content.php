<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package oquealeria
 */

?>


<article id="post-<?php the_ID(); ?>" class="inner-post <?php post_class(); ?>">
	<div class="title-post"><?php the_title(); ?></div>
	<?php
		$thumb_id = get_post_thumbnail_id();
		$thumb_url = wp_get_attachment_image_src($thumb_id,'full', true);
	?>
	<div class="hero thumb-single" style="background-image: url('<?php echo $thumb_url[0] ?>'); "></div>
	<div class="date"><?php the_date(); ?></div>
	<div class="caption -single">
		<?php the_content(); ?>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
