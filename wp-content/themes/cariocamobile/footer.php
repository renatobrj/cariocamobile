
<footer>
<div class="col-md-12">
	<div class="container">
		<div class="row">
			<div class="col-md-4 column cl-1">
				<img src="<?php bloginfo('template_url'); ?>/assets/images/logo-footer.png">
				<div class="social-footer">
					<ul>
						<li><a target="_blank" href="https://www.facebook.com/Occhialeria-282773711826811/"><i class="fa fa-facebook-square" aria-hidden="true"></i>facebook.com/occhialeria</a></li>
						<li><a target="_blank" href="https://www.instagram.com/occhialeria/"><i class="fa fa-instagram" aria-hidden="true"></i>instagram.com/occhialeria</a></li>
					</ul>
				</div>
				<div class="insta">
					<?php echo do_shortcode('[jr_instagram id="2"]'); ?>
				</div>
			</div>
			<div class="col-md-4 column cl-2">
				<div class="menu-footer">
					<ul>
						<li><a href="<?php echo site_url(); ?>/categoria-produto/feminino">FEMININO</a></li>
						<li><a href="<?php echo site_url(); ?>/categoria-produto/masculino">MASCULINO</a></li>
						<li><a href="">LOOKBOOKS</a></li>
						<li><a href="<?php echo site_url(); ?>/novidades">NOVIDADES</a></li>
					</ul>
				</div>
				<ul class="lojas-footer">
					<li>IPANEMA</li>
					<li>SHOPPING LEBLON</li>
					<li>SHOPPING VILLAGE MALL</li>
					<li>RIO DESIGN LEBLON</li>
					<li>RIO DESIGN BARRA</li>
					<li>OUTLET PREMIUM</li>
					<li>SHOPPING VILA OLÍMPIA</li>
					<li>SHOPPING ANÁLIA FRANCO</li>
				</ul>
			</div>
			<div class="col-md-4 cl-3">
				
				<div class="infos">
					<ul>
						<li><i class="fa fa-map-marker" aria-hidden="true"></i>Rua nome da ruaaaaaa nº 123 <br /> Nome do Bairro - CEP 123456 <br /> Rio de Janeiro - RJ</li>
						<li><i class="fa fa-phone" aria-hidden="true"></i>+55 21 12345-6789</li>
						<li><i class="fa fa-envelope" aria-hidden="true"></i>contato@occhialeria.com.br</li>
					</ul>
					<div class="mapa-footer">
						<a target="_blank" href=""><img src="<?php bloginfo('template_url'); ?>/assets/images/mapa-footer.png"></a>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>
</footer>
<div class="cop">
	<div class="container">
		<div class="row">
			<div class="inner-ft">
				<div>Copyright © 2018 Occhialeria. Todos os direitos reservados.</div>
				<div class="dl"><p>Criação e Desenvolvimento:</p><img src="<?php bloginfo('template_url'); ?>/assets/images/logo-dl.png"></div>
			</div>
		</div>
	</div>
</div>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="<?php bloginfo('template_url');?>/assets/js/main.min.js" rel="stylesheet"></script>
<?php wp_footer(); ?>

</body>
</html>
