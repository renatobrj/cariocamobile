<?php get_header(); ?>

<div class="header-page hero" style="background-image: url('<?php bloginfo('template_url'); ?>/assets/images/background-lojas.png');">
	<?php 
	global $wp_query;
	$pageTitle = get_the_title();
	
	$class = ($pageTitle == 'Lojas') ? 'bread-white' : '';

	?>
	<div class="woocommerce-breadcrumb <?php echo $class ?>" >
		<?php custom_breadcrumbs(); ?>
	</div>
	<h3>Nossas Lojas</h3>
</div>
<div id="lojas" class="section section-content">
	<div class="container">
		<div class="blocos-lojas">
			<div class="row">
				<div class="col-md-6" data-aos="fade-right">
					<div class="item">
						<div class="title-loja">IPANEMA</div>
						<div class="end-loja"> Rua Visconde de Pirajá, Galeria Cidade de Ipanema, 330, Lj. B <br class="trash-bloc" /> Ipanema - Rio de Janeiro - RJ - CEP 22410-002</div>
						<div class="tel">Tel.: (21) 2267-5080</div>
					</div>
				</div>
				<div class="col-md-6" data-aos="fade-left">
					<div class="item">
						<div class="title-loja">SHOPPING LEBLON</div>
						<div class="end-loja"> Av. Afrânio de Melo Franco, 290, Lj. 303A-B <br class="trash-bloc" /> Leblon - Rio de Janeiro - RJ - CEP 22430-060</div>
						<div class="tel">Tel.: (21) 2294-0144</div>
					</div>
				</div>
				<div class="col-md-6" data-aos="fade-right">
					<div class="item">
						<div class="title-loja">SHOPPING VILLAGE MALL</div>
						<div class="end-loja"> Av. das Américas, 3.900, Lj. 123, piso 1 <br class="trash-bloc" /> Barra da Tijuca - Rio de Janeiro - RJ - CEP 22640-102</div>
						<div class="tel">Tel.: (21) 3252-2515</div>
					</div>
				</div>
				<div class="col-md-6" data-aos="fade-left">
					<div class="item">
						<div class="title-loja">RIO DESIGN LEBLON</div>
						<div class="end-loja"> Av. Ataulfo de Paiva, 270, Lj. 115 <br class="trash-bloc" /> Leblon - Rio de Janeiro - RJ - CEP 22440-033</div>
						<div class="tel">Tel.: (21) 3204-1577</div>
					</div>
				</div>
				<div class="col-md-6" data-aos="fade-right">
					<div class="item">
						<div class="title-loja">RIO DESIGN BARRA</div>
						<div class="end-loja"> Av. das Américas, 7777, Lj. 122 <br class="trash-bloc" /> Barra da Tijuca - Rio de Janeiro - RJ - CEP 22793-081</div>
						<div class="tel">Tel.: (21) 3328-3801</div>
					</div>
				</div>
				<div class="col-md-6" data-aos="fade-left">
					<div class="item">
						<div class="title-loja">OUTLET PREMIUM</div>
						<div class="end-loja"> Rodovia Washington Luíz, S/N <br class="trash-bloc" />Rio de Janeiro - RJ - CEP 25230-005</div>
						<div class="tel">Tel.: (21) 3900-0500</div>
					</div>
				</div>
				<div class="col-md-6" data-aos="fade-right">
					<div class="item">
						<div class="title-loja">SHOPPING VILA OLÍMPIA</div>
						<div class="end-loja"> Rua Olimpíadas, 360, lojas 137/138 <br class="trash-bloc" /> Vila Olímpia - São Paulo - SP</div>
						<div class="tel">Tel.: (11) 3045-0616</div>
					</div>
				</div>
				<div class="col-md-6" data-aos="fade-left">
					<div class="item">
						<div class="title-loja">SHOPPING ANÁLIA FRANCO</div>
						<div class="end-loja"> Av.Regente Feijó, 1739  Loja 54B piso Lírio <br class="trash-bloc" /> Tatuapé - São Paulo - SP </div>
						<div class="tel">Tel.: (11) 2268-0630</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_template_part( 'template-parts/banner' ); ?>

<?php get_template_part( 'template-parts/noticias' ); ?>

<?php get_footer(); ?>

