module.exports = function(grunt) {
    
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-postcss');

  grunt.initConfig({
    sass: {
      dist: {
        files: {
          'assets/css/style.css': 'assets/css/sass/main.sass',
        }
      }
    },
    watch: {
      css: {
        files: ['assets/css/sass/**/*.sass'],
        tasks: ['sass','postcss']
      },
      js: {
        files: ['assets/js/*.js'],
        tasks: ['uglify']
      }
    },
    postcss: {
      options: {
        map: true, // inline sourcemaps
        processors: [
          require('pixrem')(), // add fallbacks for rem units
          require('autoprefixer')({browsers: '> 0%'}),// add vendor prefixes
           (require('cssnano')({zindex: false})) // minify the result
        ]
      },
      dist: {
        src: 'assets/css/style.css'
      }
    },
    uglify: {
      build: {
            files: {
                'assets/js/main.min.js': ['assets/js/main.js'],
            },
        }
    }
  });

grunt.registerTask('default', ['sass', 'uglify']);

};