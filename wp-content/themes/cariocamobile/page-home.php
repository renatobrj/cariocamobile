<?php get_header(); ?>

<?php get_template_part( 'template-parts/slider' ); ?>

<div class="section section-conteudo">
	<div class="header-title">
		<h2 class="title text-center">Produtos em Destaque</h2>
		<p class="text-center">Donec eu mauris lacus. Donec leo enim, aliquam quis quam eget, eleifend lobortis enim.</p>
	</div>

	<div id="gallery-prod" data-aos="fade-up" data-aos-delay="300" data-aos-duration="600">
		
		<div class="container">
			<div class="prod-home">
				<div class="prod-btn-back arrow-navigation"><img src="<?php bloginfo('template_url'); ?>/assets/images/arrow-prod-slider.png"></div>
				<div class="prod-btn-forward arrow-navigation"><img src="<?php bloginfo('template_url'); ?>/assets/images/arrow-prod-slider-fw.png"></div>
			<div class="slider-prod">
					<div class="col-md-12">
					<div class="prod-inner">
						<div class="thumb"><a href=""></a><img src="<?php bloginfo('template_url'); ?>/assets/images/prod-anne.png"></div>
						<div class="title-prod text-center"><a href="">Anne & Valentin - Nome do modelo - Código</a></div>
						<div class="link-prod text-center"><a href="">Ver Produto</a></div>
					</div>
					</div>

					<div class="col-md-12">
					<div class="prod-inner">
						<div class="thumb"><a href=""></a><img src="<?php bloginfo('template_url'); ?>/assets/images/prod-oliver.png"></div>
						<div class="title-prod text-center"><a href="">Oliver Peoples - Nome do modelo - Código</a></div>
						<div class="link-prod text-center"><a href="">Ver Produto</a></div>
					</div>
					</div>
					<div class="col-md-12">
					<div class="prod-inner">
						<div class="thumb"><a href=""></a><img src="<?php bloginfo('template_url'); ?>/assets/images/prod-persol.png"></div>
						<div class="title-prod text-center"><a href="">Oliver Peoples - Nome do modelo - Código</a></div>
						<div class="link-prod text-center"><a href="">Ver Produto</a></div>
					</div>
					</div>
					<div class="col-md-12">
					<div class="prod-inner">
						<div class="thumb"><a href=""></a><img src="<?php bloginfo('template_url'); ?>/assets/images/prod-oliver.png"></div>
						<div class="title-prod text-center"><a href="">Oliver Peoples - Nome do modelo - Código</a></div>
						<div class="link-prod text-center"><a href="">Ver Produto</a></div>
					</div>
					</div>
			</div>
			</div>
		</div>
	</div>

</div>
<div class="clearfix"></div>
<?php get_template_part( 'template-parts/banner' ); ?>

<?php get_template_part( 'template-parts/lojas' ); ?>

<?php get_template_part( 'template-parts/noticias' ); ?>

<?php get_footer(); ?>
