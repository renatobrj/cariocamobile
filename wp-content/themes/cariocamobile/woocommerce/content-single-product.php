<?php
/**
* The template for displaying product content in the single-product.php template
*
* This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
*
* HOWEVER, on occasion WooCommerce will need to update template files and you
* (the theme developer) will need to copy the new files to your theme to
* maintain compatibility. We try to do this as little as possible, but it does
* happen. When this occurs the version of the template file will be bumped and
* the readme will list any important changes.
*
* @see 	    https://docs.woocommerce.com/document/template-structure/
* @author 		WooThemes
* @package 	WooCommerce/Templates
* @version     3.0.0
*/

if ( ! defined( 'ABSPATH' ) ) {
exit; // Exit if accessed directly
}

?>

<?php
/**
* woocommerce_before_single_product hook.
*
* @hooked wc_print_notices - 10
*/
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
echo get_the_password_form();
return;
}
?>

<div class="content-single-product">
<div class="col-md-12">
	<div class="row">
	<div class="container">
	
		<div id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="row">
				
				<div class="col-md-6 order-2 order-md-1">
					<?php

					$product = new WC_product();
					$attachment_ids = $product->get_gallery_image_ids();

					foreach( $attachment_ids as $attachment_id ) 
					{
					// Display the image URL
					echo $Original_image_url = wp_get_attachment_url( $attachment_id );

					// Display Image instead of URL
					echo wp_get_attachment_image($attachment_id, 'full');

					}
					?>
					<?php
					/**
					* woocommerce_before_single_product_summary hook.
					*
					* @hooked woocommerce_show_product_sale_flash - 10
					* @hooked woocommerce_show_product_images - 20
					*/
					do_action( 'woocommerce_before_single_product_summary' );
					?>
				</div>
				
				<div class="col-md-5 offset-md-1 order-1  order-md-2">
					<div class="content-infos__prod">
						
						<div class="title-prod">
							<?php the_title(); ?>
						</div>
						
						<div class="desc-prod">
						<span>Descrição do Produto</span>
							<?php the_content(); ?>
						</div>	
						
						<p class="price price-single">
							<?php $product = wc_get_product();echo $product->get_price_html(); ?>
						</p>
					</div>
				</div>
			</div><!-- #product-<?php the_ID(); ?> -->
		</div>
	</div>
</div>
</div>
</div>
<?php do_action( 'woocommerce_after_single_product' ); ?>

