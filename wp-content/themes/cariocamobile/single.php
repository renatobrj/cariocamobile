<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package oquealeria
 */

get_header(); ?>
<div class="woocommerce-breadcrumb <?php echo $class ?>">
	<?php custom_breadcrumbs(); ?>
</div>
<div class="container">
	<div class="section section-conteudo">
		<div class="col-md-8">
		<?php
			while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', get_post_type() );

			the_post_navigation();

			endwhile; // End of the loop.
			?>
		</div>
	</div>
</div>

<?php get_template_part( 'template-parts/lojas' ); ?>

<?php get_template_part( 'template-parts/banner' ); ?>

<?php

get_footer();
