<?php get_header(); ?>

<div class="woocommerce-breadcrumb <?php echo $class ?>" >
	<?php custom_breadcrumbs(); ?>
</div>
<div id="contato" class="section section-conteudo">
	<div class="formulario">
		<div class="container">
			
				<!-- <div class="col-md-6">
					<div class="item-form">
						<label>Nome</label>
						<input type="text" name="">
					</div>
					<div class="item-form">
						<label>E-mail</label>
						<input type="text" name="">
					</div>
					<div class="item-form">
						<label>Telefone</label>
						<input type="text" name="">
					</div>
				</div>
				<div class="col-md-6">
					<div class="item-form">
						<label>Mensagem</label>
						<textarea></textarea>
					</div>
				</div>
				<div class="item-send">
					<input type="submit" name="" value="ENVIAR">
				</div> -->
				<?php echo do_shortcode('[contact-form-7 id="73" title="Contato"]'); ?>
		
		</div>
	</div>
</div>
<div class="maps embed-responsive embed-responsive-16by9">
<iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3672.6155793894013!2d-43.392387850182914!3d-23.00115948489008!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9bdb992fb6c4d1%3A0x765d8ec1953b222e!2sAv.+das+Am%C3%A9ricas%2C+7935+-+Barra+da+Tijuca%2C+Rio+de+Janeiro+-+RJ%2C+22793-081!5e0!3m2!1spt-BR!2sbr!4v1515430217292" width="100%" height="115" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
<?php get_template_part( 'template-parts/banner' ); ?>

<?php get_template_part( 'template-parts/lojas' ); ?>

<?php get_template_part( 'template-parts/noticias' ); ?>

<?php get_footer(); ?>

