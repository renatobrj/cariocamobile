<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações
// com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */

define('WP_HOME','http://localhost/occhialeria');
define('WP_SITEURL','http://localhost/occhialeria');

define('DB_NAME', 'occhialeria');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

define('FS_METHOD','direct');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ')f,msO?u+nr0>QRtq_/msiwzv^Ebcg}[>Y,nC&&@%Pr^ednf:[`6TM{+W6gX$E7m');
define('SECURE_AUTH_KEY',  '54nu6yTp}kA:#bcdKk*moGEN<,<E=n|RaJGA_I@Sl~`<,9)W}D?eEW_m;gn`XPJU');
define('LOGGED_IN_KEY',    'jpTSbq|z,MJgz;SEi282O5n7Q$1 8{ZrS#1/Q5`Ih/_`WX5~W#1-%qs5aXHa.x I');
define('NONCE_KEY',        ' Te~PX~{(X@5?aLZpjeo]N5}P=:1_{;wL>s3$1#4n1j3?U[5A#<ny.x,*,x[gYsl');
define('AUTH_SALT',        '[52@ctnPaPhQHm]TTs*9& tz|{ e&QzN6D*QU^3^qqhgjN@Qf:BaT=O7BGXWzd~n');
define('SECURE_AUTH_SALT', '/qd:`:xAYj|.Xv*;a_8~h`0T@/~>hS7kpc0@c=z22ms%s:{Wa#=Hn6+(1)[% 9B`');
define('LOGGED_IN_SALT',   '.o{:5??V=L^:%bn:9X>.B!vw=+.# O)BsC`mW:c;LZwG.fx ;x8H0ZI0-*+p<3xr');
define('NONCE_SALT',       '4gX10Kwh{wn5]`GiQzJ?~$BbPcK#@sqNIPxG;N[_EF).Tk=9|ea@o|9znm>C2z<5');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
